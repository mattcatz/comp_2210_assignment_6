import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class LostForWordsTest {

   private final String DEFAULT_LEXICON = "wordfiles/words.txt";
   private String[] _20x20 = new String[]{"O","Y","D","D","T","P","N","R","A","H","E","L","C","S","B","P","S","U","B","G","U","P","Y","H","R","R","X","R","E","F","H","D","H","T","K","X","K","O","Z","F","W","Y","H","Y","T","C","H","M","V","P","R","T","A","K","N","E","S","I","B","T","M","V","Y","Q","E","U","O","E","F","A","K","J","C","W","I","K","I","U","K","T","P","O","F","E","G","Z","T","X","O","Z","T","H","K","B","M","G","D","P","P","P","G","U","E","S","C","J","C","B","Q","F","T","R","I","P","N","I","E","W","P","K","H","K","G","B","B","L","Y","J","P","J","E","O","N","Q","V","N","B","S","H","R","N","Z","R","G","A","E","W","P","L","L","Z","R","G","I","E","T","U","N","R","L","I","K","T","J","K","J","F","C","I","T","M","R","D","T","R","E","G","L","J","G","I","K","H","L","C","V","P","P","D","S","Q","E","W","O","C","R","L","V","L","P","T","A","T","N","O","R","M","W","K","O","D","O","U","O","V","F","M","H","V","V","S","I","X","Z","L","O","T","Z","L","B","R","G","F","Q","P","A","Y","P","D","L","B","K","S","N","C","H","O","P","Y","K","H","C","R","R","I","C","S","B","J","X","R","F","I","Y","R","H","B","Z","I","P","C","K","I","N","O","E","C","C","U","C","P","I","J","R","E","Y","E","Z","U","R","R","M","F","S","M","R","N","J","I","B","T","Q","O","C","V","R","O","T","X","H","C","R","W","S","A","V","T","N","U","I","O","W","X","C","O","R","X","Q","A","S","A","S","S","E","M","B","L","Y","O","Z","F","P","L","S","C","I","T","L","U","M","O","N","I","T","O","R","J","W","I","N","L","L","L","E","L","J","R","R","E","M","M","O","B","D","X","I","J","D","S","R","L","C","H","S","H","Y","U","L","P","M","O","U","S","E","C","B","I","I","U","I",};

   @Test(expected = IllegalArgumentException.class)
   public void testLoadLexicon() {
      LostForWords game = new LostForWords();
      game.loadLexicon("ham");
   }

   @Test(expected = IllegalStateException.class)
   public void testGetAllValidWords_NoLexicon() {
      LostForWords game = new LostForWords();
      game.getAllValidWords(1);
   }

   @Test
   public void testGetAllValidWords_Default() {
      try {
         LostForWords game = new LostForWords();
         game.loadLexicon(DEFAULT_LEXICON);
         TreeSet<String> expected = getValidDefault5();
         SortedSet<String> results = game.getAllValidWords(5);
         for (String s : results) {
            Assert.assertTrue(expected.contains(s));
         }
      }catch (Exception e) {
         Assert.fail("An unexpected error occurred");
      }
   }

   @Test
   public void testGetAllValidWords_20x20() {
      try {
         LostForWords game = new LostForWords();
         game.setBoard(_20x20);
         game.loadLexicon(DEFAULT_LEXICON);
         SortedSet<String> results = game.getAllValidWords(10);
      }catch (Exception e) {
         e.printStackTrace();
      }
   }

   @Test(expected = IllegalStateException.class)
   public void testGetScoreForWords_NoLexicon() {
      LostForWords game = new LostForWords();
      game.getScoreForWords(null, 8);
   }

   @Test
   public void testGetScoreForWords_5() {
      try {
         LostForWords game = new LostForWords();
         game.loadLexicon(DEFAULT_LEXICON);
         int results = game.getScoreForWords(getValidDefault5(), 5);
         int expected = 31;
         Assert.assertEquals(expected, results);
      }catch (Exception e) {
         Assert.fail("An unexpected error occurred");
      }
   }

   @Test(expected = IllegalStateException.class)
   public void testIsValidWord_NoLexicon() {
      LostForWords game = new LostForWords();
      game.isValidWord("matt");
   }

   @Test(expected = IllegalStateException.class)
   public void testIsValidPrefix_NoLexicon() {
      LostForWords game = new LostForWords();
      game.isValidPrefix("matt");
   }

   @Test
   public void testIsOnBoard_Default_Peace() {
      LostForWords game = new LostForWords();
      game.loadLexicon(DEFAULT_LEXICON);
      List<Integer> output = game.isOnBoard("peace");
      StringBuilder results = new StringBuilder();
      for (Integer i : output) {
         results.append(i);
      }
      Assert.assertEquals("76321", results.toString());
   }

   @Test
   public void testIsOnBoard_Default_Lent() {
      LostForWords game = new LostForWords();
      game.loadLexicon(DEFAULT_LEXICON);
      List<Integer> output = game.isOnBoard("lent");
      StringBuilder results = new StringBuilder();
      for (Integer i : output) {
         results.append(i);
      }
      Assert.assertEquals("56913", results.toString());
   }

   @Test
   public void testIsOnBoard_Default_Not_Found() {
      LostForWords game = new LostForWords();
      game.loadLexicon(DEFAULT_LEXICON);
      List<Integer> output = game.isOnBoard("Matthew");
      Assert.assertTrue(output.isEmpty());
   }

   private TreeSet<String> getValidDefault5() {
      TreeSet<String> output = new TreeSet<String>();
      output.add("ALBEE");
      output.add("ALCAE");
      output.add("ALEPOT");
      output.add("ANELE");
      output.add("BECAP");
      output.add("BELAH");
      output.add("BELEE");
      output.add("BENTHAL");
      output.add("BENTY");
      output.add("BLENT");
      output.add("CAPEL");
      output.add("CAPOT");
      output.add("CENTO");
      output.add("CLEAN");
      output.add("ELEAN");
      output.add("LEANT");
      output.add("LENTH");
      output.add("LENTO");
      output.add("NEELE");
      output.add("PEACE");
      output.add("PEELE");
      output.add("PELEAN");
      output.add("PENAL");
      output.add("THANE");
      output.add("TOECAP");
      output.add("TOPEE");
      return output;
   }
}
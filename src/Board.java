import java.util.LinkedList;

public class Board{
   private int side;
   private Cell[] board;

   public Board() {
      this(new String[]{"E", "E", "C", "A", "A", "L", "E", "P", "H", "N", "B", "O", "Q", "T", "T", "Y"});
   }

   public Board(String[] letterArray) {
      if (letterArray == null) throw new IllegalArgumentException("Must not be null");
      if (letterArray.length == 0) throw new IllegalArgumentException("Must not be empty");
      side = (int) Math.sqrt(letterArray.length);
      if (side*side != letterArray.length) {
         throw new IllegalArgumentException("Must be a perfect square!!!");
      }
      board = new Cell[letterArray.length];
      for(int x = 0; x < side; x++) {
         for(int y = 0; y < side; y++) {
            board[x * side + y] = new Cell(x,y,letterArray[x * side + y]);
         }
      }
   }

   public int getSideLength() {
      return side;
   }

   public Cell getCell(int x, int y) {
      return board[x * side + y];
   }

   public LinkedList<Cell> getNeighbors(Cell cell) {
      LinkedList<Cell> neighbors = new LinkedList<Cell>();
      int x = cell.getX();
      int y = cell.getY();
      boolean temp1 = x > 0;
      boolean temp2 = x < side - 1;
      boolean temp3 = y > 0;
      boolean temp4 = y < side - 1;
      if (temp2) {
         neighbors.add(getCell(x + 1, y));
         if (temp3) {
            neighbors.add(getCell(x + 1, y - 1));
         }
         if (temp4) {
            neighbors.add(getCell(x + 1, y + 1));
         }
      }
      if (temp1) {
         neighbors.add(getCell(x - 1, y));
         if (temp3) {
            neighbors.add(getCell(x - 1, y - 1));
         }
         if (temp4) {
            neighbors.add(getCell(x - 1, y + 1));
         }
      }
      if (temp3) neighbors.add(getCell(x, y - 1));
      if (temp4) neighbors.add(getCell(x, y + 1));
      return neighbors;
   }

   @Override
   public String toString() {
      StringBuilder output = new StringBuilder();
      for (int i = 0; i < board.length; i++) {
         if (i > 0 && i % side == 0) {
            output = output.append("\n");
         }
         output = output.append(board[i].getValue());
      }
      return output.toString();
   }

   @Override
   public boolean equals(Object o) {
      if (o.equals(this)) return true;
      if (o instanceof Board) return this.toString().equals(((Board) o).toString());
      return false;
   }
}